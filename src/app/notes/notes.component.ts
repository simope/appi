import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements OnInit {
  notes = [
    {title: 'Chores',
    value: 'Don\'t forget to clean up',
    color: 'lightblue'}, 
    {title: 'walk',
    value: 'oooooeeeeeeee',
    color: 'yellow'},
    {title: 'dishes',
    value: 'do them',
    color: 'red'}
    ];

  onNoteChecked(i:number) {
    this.notes.splice(i,1);
  }

  ngOnInit() {
  }

}
