import { AppiPage } from './app.po';

describe('appi App', function() {
  let page: AppiPage;

  beforeEach(() => {
    page = new AppiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
